﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="uploadify/uploadify.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="fl1" runat="server" />
    </div>
    </form>
</body>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="uploadify/jquery.uploadify.js"></script>
<script>
    $(function () {
        $("#<%= fl1.ClientID%>").uploadify({
            height: 15,
            swf: '/uploadify/uploadify.swf',
            uploader: '/uploadify/uploadify.ashx',
            width: 80,
        });
    });
</script>
</html>
